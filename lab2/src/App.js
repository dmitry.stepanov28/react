import Task1 from "./tasks/Task1";
import Task2 from "./tasks/Task2";
import Task3 from "./tasks/Task3";
import Task4 from "./tasks/Task4";

function App() {
    const counters = [
        {id: 1, value: 1, min: -9, max: 6},
        {id: 2, value: 3, min: -15},
        {id: 3, value: 7},
    ]
    const products = [
        {id: 0, name:"Mouse", value: 3, min: -3, max: 6, price: 100},
        {id: 1, name:"Keyboard", value: 4, min: -5, max: 6, price: 200},
        {id: 2, name:"Monitor", value: 4, price: 300},
    ]
    return (
        <div className="App">
            {counters.map(counter =>
                <Task1 key={counter.id} value={counter.value} min={counter.min} max={counter.max}/>
            )}
            <Task2 />
            <Task3 products={products}/>
            <Task4 />
        </div>
    );
}

export default App;
