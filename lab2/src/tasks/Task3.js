import React, {Component} from 'react';
import {Button, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";

class Task3 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: props.products.map(product =>
                (product.min === undefined && product.max === undefined) ? {
                        ...product,
                        min: 0,
                        max: 9999999999,
                    }
                    : (product.max === undefined && product.min !== undefined) ? {
                        ...product,
                        max: 9999999999,
                    } : (product.max !== undefined && product.min === undefined) ? {
                            ...product,
                            min: 0,
                        } : product)
        }
        this.handleIncrement = this.handleIncrement.bind(this)
        this.handleDecrement = this.handleDecrement.bind(this)
        this.sumCount = this.sumCount.bind(this)
        this.sumCost = this.sumCost.bind(this)
    }

    handleIncrement(id) {
        if (this.state.products[id].value < this.state.products[id].max) {
            this.setState(state => {
                return {
                    products: state.products.map(product => product.id === id ? {
                        ...product,
                        value: product.value + 1
                    } : product)
                }
            })
        }
    }

    handleDecrement(id) {
        if (this.state.products[id].value > this.state.products[id].min) {
            this.setState(state => {
                return {
                    products: state.products.map(product => product.id === id ? {
                        ...product,
                        value: product.value - 1
                    } : product)
                }
            })
        }
    }

    sumCount(arr){
        let sum = 0;
        for(let i = 0; i < arr.length; i++){
            sum += arr[i].value
        }
        return sum
    }

    sumCost(arr){
        let sum = 0;
        for(let i = 0; i < arr.length; i++){
            sum += arr[i].value * arr[i].price
        }
        return sum
    }

    render() {
        return (
            <TableContainer sx={{mx: 2}}>
                <TableHead>
                    <TableRow sx={{bgcolor: "#4db6ac"}}>
                        <TableCell><b>Name</b></TableCell>
                        <TableCell align="center"><b>Price</b></TableCell>
                        <TableCell align="center"><b>Quantity</b></TableCell>
                        <TableCell align="center"><b>Total</b></TableCell>
                    </TableRow>
                </TableHead>
                {this.state.products.map((product) => (
                    <TableRow key={product.id}>
                        <TableCell>{product.name}</TableCell>
                        <TableCell align="center">{product.price}</TableCell>
                        <TableCell align="center">
                            <Button variant="outlined" sx={{mx: 1}}
                                    onClick={() => this.handleIncrement(product.id)}>+</Button>
                            {product.value}
                            <Button variant="outlined" sx={{mx: 1}}
                                    onClick={() => this.handleDecrement(product.id)}>-</Button>
                        </TableCell>
                        <TableCell align="center">{product.price * product.value}</TableCell>
                    </TableRow>
                ))}
                <TableHead>
                    <TableRow sx={{bgcolor: "#81d4fa"}}>
                        <TableCell><b>Totals</b></TableCell>
                        <TableCell align="center"></TableCell>
                        <TableCell align="center"><b>{this.sumCount(this.state.products)}</b></TableCell>
                        <TableCell align="center"><b>{this.sumCost(this.state.products)}</b></TableCell>
                    </TableRow>
                </TableHead>
            </TableContainer>
        );
    }
}

export default Task3;