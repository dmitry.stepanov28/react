import React, {Component} from 'react';
import {Button} from "@mui/material";

class Task1 extends Component {
    static defaultProps = {
        value: 0,
        min: -10,
        max: 10,
    };

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value,
            min: this.props.min,
            max: this.props.max,
        }
        this.handleIncrement = this.handleIncrement.bind(this)
        this.handleDecrement = this.handleDecrement.bind(this)
        this.handleReset = this.handleReset.bind(this)
    }

    handleIncrement() {
        if (this.state.value < this.props.max) {
            this.setState({
                value: this.state.value + 1
            })
        }
    }

    handleDecrement() {
        if (this.state.value > this.props.min) {
            this.setState({
                value: this.state.value - 1
            })
        }
    }

    handleReset() {
        this.setState({
            value: 0
        })
    }


    render() {
        return (
            <div>
                Поточний рахунок: {this.state.value}
                <Button variant="outlined" sx={{ mx: 1 , my:1}} onClick={this.handleIncrement}>+</Button>
                <Button variant="outlined" sx={{ mx: 1 }} onClick={this.handleDecrement}>-</Button>
                <Button variant="outlined" sx={{ mx: 1 }} onClick={this.handleReset}>Reset</Button>
            </div>
        );
    }
}

export default Task1;