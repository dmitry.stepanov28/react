import React, {Component} from 'react';
import {Button, List, ListItem, ListItemText} from '@mui/material';

class Task2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            id: 0,
            name: ""
        }
        this.inputName = this.inputName.bind(this)
        this.addProduct = this.addProduct.bind(this)
        this.delete = this.delete.bind(this)
    }

    inputName(event) {
        this.setState({
            name: event.target.value
        })
    }

    addProduct(event) {
        event.preventDefault();
        this.setState((state) => {
            let product = {
                id: state.id + 1,
                name: state.name
            }
            let products = [...state.products, product]
            return {
                id: state.id + 1,
                products: products
            }
        })
    }

    delete(id) {
        this.setState({
            products: this.state.products.filter(product => product.id !== id)
        })
    }


    render() {
        return (
            <div>
                <form onSubmit={this.addProduct}>
                    <input value={this.state.name} onChange={this.inputName}/>
                    <Button type="submit" sx={{mx: 2}} size={"small"} variant="outlined">Додати</Button>
                </form>
                <List sx={{maxWidth: 260}}>
                    {this.state.products.map((product) => (
                        <ListItem key={product.id}>
                            <ListItemText>{product.name}</ListItemText>
                            <Button variant="outlined" size="small" color="error"
                                    onClick={() => this.delete(product.id)}>🗑</Button>
                        </ListItem>
                    ))}
                </List>
            </div>
        );
    }
}

export default Task2;