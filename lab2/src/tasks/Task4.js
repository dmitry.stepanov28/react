import React, {Component} from 'react';
import {Button, Container} from "@mui/material";

class Task4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            attempts: 0,
            result: "",
            answer: 0,
            state: false,
            trueAnswer: 0,
            condition: []
        }
        this.newGame = this.newGame.bind(this)
        this.setAnswer = this.setAnswer.bind(this)
        this.checkAnswer = this.checkAnswer.bind(this)
        this.setCondition = this.setCondition.bind(this)
    }

    newGame() {
        this.setState({
            state: true,
            trueAnswer: Math.floor(Math.random() * (1000 - 1) + 1),
            result: "",
        })
    }

    setAnswer(event) {
        this.setState({
            answer: event.target.value
        })
    }

    checkAnswer() {
        if (parseInt(this.state.answer) === this.state.trueAnswer) {
            this.setState({
                result: "You won the game!",
                attempts: 0,
                state: false,
                condition: [],
                answer: 0,
            })
        } else {
            if (this.state.attempts !== 9)
                this.setState({
                    result: "Wrong answer!",
                    attempts: this.state.attempts + 1,
                    condition: [...this.state.condition, this.setCondition(this.state.answer)]
                })
            else {
                this.setState({
                    result: "You lost the game!",
                    attempts: 0,
                    state: false,
                    condition: [],
                    answer: 0,
                })
            }
        }
    }

    setCondition(answer){
        if (answer > this.state.trueAnswer)
            return answer + " > N"
        if (answer < this.state.trueAnswer)
            return answer + " < N"

    }

    render() {
        return (
            <Container sx={{mx: 2, my: 3}}>
                <Button variant="contained" color="success" sx={{mx: 1}} onClick={this.newGame} disabled={this.state.state} size="small">New Game</Button>
                <input onChange={this.setAnswer} disabled={!this.state.state} value={this.state.answer}/>
                <Button variant="contained" onClick={this.checkAnswer} sx={{mx: 1}}  disabled={!this.state.state} size="small">Check</Button>
                <br></br>
                <br></br>
                Information:
                <Container>
                    {this.state.condition.map((condition, index) => (
                        <h5 key={index}>{condition}</h5>
                    ))}
                </Container>
                <br></br>
                Attempts: {this.state.attempts}
                <br></br>
                <br></br>
                Result: {this.state.result}
            </Container>
        );
    }
}

export default Task4;