import React, {useEffect, useRef, useState} from 'react';
import {Container, Input} from "@mui/material";
import PhotoItem from "./PhotoItem";
import ReactPaginate from "react-paginate";


function PhotoApp(props) {
    const [photos, setPhotos] = useState({
        photos: props.photos,
        sorted: [],
        sortedValue: 2
    });

    const list = useRef(null);

    let array = []

    const [counts, setCounts] = useState(photos.photos.length);

    const [scroll, setScroll] = useState(1);

    const [filtered, setFiltered] = useState([]);

    const [styleList, setStyleList] = useState({
            overflow: "scroll",
            width: 700,
            height: 500,
            padding: 5,
            border: "solid 1px black"
        }
    )
    const [styleRow, setStyleRow] = useState({
            height: 200
        }
    )

    const [itemOffset, setItemOffset] = useState(0);

    const endOffset = itemOffset + (props.itemsPerPage > (counts - itemOffset) ? (counts - itemOffset) : props.itemsPerPage);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * props.itemsPerPage) % photos.photos.length;
        setItemOffset(newOffset);
    };


    const sort = (event) => {
        switch (event === undefined ? photos.sortedValue.toString() : event.target.value) {
            case "1":
                setPhotos(items => ({
                        ...items,
                        photos: items.photos.sort(function (a, b) {
                            if (a.albumId < b.albumId) {
                                return 1;
                            }
                            if (a.albumId > b.albumId) {
                                return -1;
                            }
                            return 0;
                        }),
                        sortedValue: 1
                    }
                ))
                break;
            case "2":
                setPhotos(items => ({
                        ...items,
                        photos: items.photos.sort(function (a, b) {
                            if (a.albumId > b.albumId) {
                                return 1;
                            }
                            if (a.albumId < b.albumId) {
                                return -1;
                            }
                            return 0;
                        }),
                        sortedValue: 2
                    })
                )
                break;
            case "3":
                setPhotos(items => ({
                        ...items,
                        photos: items.photos.sort(function (a, b) {
                            if (a.title < b.title) {
                                return 1;
                            }
                            if (a.title > b.title) {
                                return -1;
                            }
                            return 0;
                        }),
                        sortedValue: 3
                    }
                ))
                break;

            case "4":
                setPhotos(items => ({
                        ...items,
                        photos: items.photos.sort(function (a, b) {
                            if (a.title > b.title) {
                                return 1;
                            }
                            if (a.title < b.title) {
                                return -1;
                            }
                            return 0;
                        }),
                        sortedValue: 4
                    }
                ))
                break;
            default:
                break;
        }
    }

    const countPhotos = (event) => {
        setItemOffset(0)
        if (parseInt(event.target.value) >= 1 && parseInt(event.target.value) <= props.photos.length)
            setCounts(event.target.value)
        else if (parseInt(event.target.value) > props.photos.length)
            setCounts(props.photos.length)
        else
            setCounts(1)
    }

    const listHeight = (event) => {
        if (parseInt(event.target.value) >= 1)
            setStyleList({
                ...styleList,
                height: parseInt(event.target.value)
            })
        else
            setStyleList({
                ...styleList,
                height: 1
            })
    }

    const scrollTo = (event) => {
        if (parseInt(event.target.value) >= 1 && parseInt(event.target.value) <= props.itemsPerPage && parseInt(event.target.value) <= (counts - itemOffset)) {
            setScroll(parseInt(event.target.value))
            list.current.scrollTo(0, 200 * (parseInt(event.target.value) - 1))
        } else {
            setScroll(1)
        }
    }

    const rowHeight = (event) => {
        if (parseInt(event.target.value) >= 1)
            setStyleRow({
                ...styleRow,
                height: parseInt(event.target.value)
            })
        else
            setStyleRow({
                ...styleRow,
                height: 1
            })
    }

    useEffect(() => {
        if (filtered.length !== 0)
            filter1()
    },)

    const filter = (event) => {
        if (event.target.checked)
            setFiltered(
                [...filtered, event.target.value]
            )
        else if (filtered.includes(event.target.value)) {
            let array = filtered
            array.splice(filtered.indexOf(event.target.value), 1)
            if (array.length === 0)
                filter1()
            setFiltered(array)
        }
    }

    function filter1() {
        let array = []
        for (let i = 0; i < props.photos.length; i++) {
            if (filtered.includes(props.photos[i].albumId.toString())) {
                array.push(props.photos[i])
            }
        }
        if (array.length === 0) {
            setPhotos({
                ...photos,
                photos: props.photos
            })
            setCounts(props.photos.length)
            sort()
            return
        }
        setPhotos({
            ...photos,
            photos: array
        })
        setCounts(array.length)
        sort()
    }


    return (
        <Container>
            <table>
                <tr>
                    <td>Nums Rows
                        <br></br>
                        <Input onChange={countPhotos} value={counts}/>
                    </td>
                    <td>List height
                        <br></br>
                        <Input onChange={listHeight} value={styleList.height}/>
                    </td>
                    <td>Row height
                        <br></br>
                        <Input onChange={rowHeight} value={styleRow.height}/>
                    </td>
                    <td>Scroll to
                        <br></br>
                        <Input onChange={scrollTo} value={scroll}/>
                    </td>
                    <td>Sort by
                        <br></br>
                        <select onChange={sort}>
                            <option value={1}>Album upper</option>
                            <option value={2} selected>Album bottom</option>
                            <option value={3}>Title upper</option>
                            <option value={4}>Title bottom</option>
                        </select>
                    </td>
                </tr>
            </table>
            <div className="row">
                <div className="col-8">
                    <Items
                        currentItems={
                            photos.photos.slice(itemOffset, endOffset)
                        }
                        styleList={styleList} list={list}
                        styleRow={styleRow}/>
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel="next >"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={Math.ceil(counts / props.itemsPerPage)}
                        previousLabel="< previous"
                        renderOnZeroPageCount={null}
                    />
                </div>
                <div className="col-4">
                    Filter by album id
                    <br></br>
                    {
                        props.photos.map((photo) => {
                                return (!array.includes(photo.albumId) && array.push(photo.albumId) && <div>
                                    <input type="checkbox" onChange={filter} key={photo.albumId}
                                           value={photo.albumId}/>{photo.albumId}
                                </div>)
                            }
                        )
                    }
                </div>
            </div>
        </Container>
    );

}

function Items({currentItems, styleList, list, styleRow}) {

    return (
        <div id="menu" style={styleList} ref={list}>
            {
                currentItems && currentItems.map((photo) =>
                    <PhotoItem key={photo.id} photo={photo} style={styleRow}/>
                )
            }
        </div>
    );
}


export default PhotoApp;