import React from "react";
import {Grid} from "@mui/material";
import {Image} from "react-bootstrap";

function PhotoItem(props) {

    return (
        <Grid
            container
            direction="row"
            alignItems="center"
            style={props.style}
            sx={{mx: 2}}
        >
            <a href={props.photo.url} className="mx-2">
                <Image roundedCircle={true} src={props.photo.thumbnailUrl}/>
            </a>
            <div className="mx-2">
                {props.photo.title}
            </div>
            <hr></hr>
        </Grid>
    );


}

export default PhotoItem;