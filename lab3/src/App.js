import './App.css';
import PhotoApp from "./components/PhotoApp";
import {photos} from "./components/photos";

function App() {
    let array = []
    for (let i = 0; i < photos.length; i++) {
        if (photos[i].title.split(" ").length <= 7) {
            array.push(photos[i])
        }
    }
    return (
        <div className="App">
            <PhotoApp itemsPerPage={10} photos={array}/>
        </div>
    );
}

export default App;
