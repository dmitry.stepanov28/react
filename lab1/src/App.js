import './App.css';
import Task1 from "./task/Task1";
import Task2 from "./task/Task2";
import Task3 from "./task/Task3";
import Task4 from "./task/Task4";
import React from "react";


function App() {
    const cities = [
        {id: 1, name: "Chicago"},
        {id: 2, name: "Los Angelos"},
        {id: 3, name: "New York"}
    ]
    const colors = [
        {name: "red"},
        {name: "black"},
        {name: "white"}
    ]
    return (
        <div>
            <Task1/>
            <br></br>
            <Task2 product={{name: "Mouse", id: 1}}/>
            <Task2 product={{name: "Keyboard", id: 2}}/>
            <br></br>
            <Task3 cities={cities}/>
            <Task4 colors={colors}/>
        </div>
    );
}

export default App;
