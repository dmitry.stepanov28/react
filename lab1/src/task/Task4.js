import React, {Component} from 'react';

class Task4 extends Component {
    constructor(props) {
        super(props);
        this.state = {color: "red"}
        this.changeColor = this.changeColor.bind(this)
    }

    changeColor(event) {
        this.setState({
            color: event.target.value
        })
    }

    render() {
        return (
            <div>
                <h2>I am a {this.state.color} product!</h2>
                <select onChange={this.changeColor}>
                    {this.props.colors.map((color, index) =>
                        <option key={index}
                                value={color.name}>{color.name}</option>
                    )}
                </select>
            </div>
        );
    }
}

export default Task4;