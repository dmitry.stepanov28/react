import React from 'react';

function Task3(props) {

    return (
        <div>
            <select>
                {props.cities.map((city) =>
                    <option key={city.id}
                            value={city.name} >{city.name}</option>
                )}
            </select>
        </div>
    );
}

export default Task3;