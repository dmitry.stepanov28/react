import React from 'react';
import './Task1.css';

function Task1() {
    return (
        <div>
          <table>
              <tr>
                  <td>
                      First Name
                  </td>
                  <td>
                      John
                  </td>
              </tr>
              <tr>
                  <td>
                      Last Name
                  </td>
                  <td>
                      Silver
                  </td>
              </tr>
              <tr>
                  <td>
                      Occupation
                  </td>
                  <td>
                      Pirate Captain
                  </td>
              </tr>
          </table>
        </div>
    );
}

export default Task1;

