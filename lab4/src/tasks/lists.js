import React from 'react';
import {Button, List, ListItem, ListItemText} from "@mui/material";

function Lists({products, deleteItem}) {
    // console.log(products)
    return (
        <div>
            <List sx={{maxWidth: 260}}>
                {products?.map((product, index) => (
                    <ListItem key={product.id}>
                        <ListItemText>{product.name}</ListItemText>
                        <Button variant="contained" size="small" color="error"
                                onClick={() => {
                                    deleteItem (index)
                                }}>Delete</Button>
                    </ListItem>
                ))}
            </List>
        </div>
    );
}

export default Lists;