import React, {useEffect, useReducer, useState} from 'react';
import {Button, Input} from '@mui/material';
import Lists from "./lists"


const reducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_PRODUCTS':
            return {
                products: action.products
            };
        case 'ADD_PRODUCT':
            return {
                products: [...state.products, action.newItem]
            };
        case 'REMOVE_PRODUCT':
            return {
                products: state.products.filter((item, index) => index !== action.id)
            };
    }
}

const Task2 = () => {

    const data = JSON.parse(localStorage.getItem('data')) || [];
    const [state, dispatch] = useReducer(reducer, {products: []});
    const [value, setValue] = useState(' ');

    useEffect(() => {
        dispatch({type: 'POPULATE_PRODUCTS', products: data});
    }, []);

    useEffect(() => {
        localStorage.setItem('data', JSON.stringify(state?.products));
    }, [state]);

    const removeItem = index => {
        dispatch({type: 'REMOVE_PRODUCT', id: index});
    };

    return (
        <div>
            <input value={value} onChange={e => setValue(e.target.value)}/>
            <Button type="submit" sx={{mx: 2}} size={"small"} variant="outlined" onClick={() => {
                dispatch({type: 'ADD_PRODUCT', newItem: {id: state.products.length, name: value}});
                // setData(prev => [...prev, {id: data.length, name: value}]);
                setValue(' ');
            }}>Add</Button>
            {state &&
                <Lists products={state.products} deleteItem={removeItem}/>
            }
        </div>
    );

}

export default Task2;