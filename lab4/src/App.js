import Task2 from "./tasks/Task2";

function App() {
    return (
        <div className="App">
            <Task2 />
        </div>
    );
}

export default App;
